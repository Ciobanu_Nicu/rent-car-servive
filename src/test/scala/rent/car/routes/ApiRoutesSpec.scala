package rent.car.routes

import akka.http.scaladsl.model._
import akka.http.scaladsl.server._
import akka.http.scaladsl.testkit.ScalatestRouteTest
import cats.effect.IO
import org.scalatest.flatspec.AnyFlatSpec
import rent.car.database.CarRentRepository
import rent.car.model.RentCarModel
import rent.car.model.RentCarModel.{BodyType, Car, CarBrand, Contract, Customer, FuelType}
import spray.json._
import rent.car.model.RentCarJsonMarshalling._

import java.time.LocalDate

class ApiRoutesSpec extends AnyFlatSpec with ScalatestRouteTest {

  "ApiRoutes" should
    "return OK when car exists" in new TestSubject {
      Get("/api/get/9999/car") ~> Route.seal(routes) ~> check {
        assert(status == StatusCodes.OK)
        assert(entityAs[String] == car.toJson.toString)
      }
    }

  it should "return OK when customer exists" in new TestSubject {
    Get("/api/get/1/customer") ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.OK)
      assert(entityAs[String] == customer.toJson.toString)
    }
  }

  it should "return NotFound when car doesn't exists" in new TestSubject {
    Get("/api/get/9998/car") ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.NotFound)
    }
  }

  it should "return NotFound when customer doesn't exists" in new TestSubject {
    Get("/api/get/2/customer") ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.NotFound)
    }
  }

  it should "return Internal Server Error when there is an error while retrieving a car" in new TestSubject {
    Get("/api/get/9997/car") ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.InternalServerError)
    }
  }

  it should "return Internal Server Error when there is an error while retrieving a customer" in new TestSubject {
    Get("/api/get/3/customer") ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.InternalServerError)
    }
  }

  it should "return OK when car is added successfully" in new TestSubject {
    Post("/api/add/car", car) ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.OK)
    }
  }

  it should "return OK when customer is added successfully" in new TestSubject {
    Post("/api/add/customer", customer) ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.OK)
    }
  }

  it should "return Unsupported Media Type when the car input is malformed" in new TestSubject {
    Post("/api/add/car", "Unsupported Media Type") ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.UnsupportedMediaType)
    }
  }

  it should "return Unsupported Media Type when the customer input is malformed" in new TestSubject {
    Post("/api/add/customer", "Unsupported Media Type") ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.UnsupportedMediaType)
    }
  }

  it should "return Internal Server Error when there is an error while adding a car" in new TestSubject {
    val request = car.copy(id = "9998")
    Post("/api/add/car", request) ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.InternalServerError)
    }
  }

  it should "return Internal Server Error when there is an error while adding a customer" in new TestSubject {
    val request = customer.copy(id = "3")
    Post("/api/add/customer", request) ~> Route.seal(routes) ~> check {
      assert(status == StatusCodes.InternalServerError)
    }
  }

  trait TestSubject {

    val car = Car(
      id = "9999",
      brand = CarBrand.Toyota,
      fuel = FuelType.Hybrid,
      body = BodyType.Sedans,
      year = LocalDate.now()
    )

    val customer = Customer(
      id = "1",
      firstName = "Dave",
      lastName = "Smith",
      birth = LocalDate.parse("1988-04-12")
    )

    val routes: Route = new ApiRoutes {
      override val carRentRepository: CarRentRepository[IO] = new CarRentRepository[IO] {
        override def saveCar(car: RentCarModel.Car): IO[Unit] =
          if (car.id == "9999") IO.unit
          else IO.raiseError(new Exception("Internal Server Error"))

        override def getCar(carId: String): IO[Option[RentCarModel.Car]] = {
          if (carId == "9999") IO.pure(Some(car))
          else if (carId == "9998") IO.pure(None)
          else IO.raiseError(new Exception("Internal Server Error"))
        }

        override def saveCustomer(customer: RentCarModel.Customer): IO[Unit] =
          if (customer.id == "1") IO.unit
          else IO.raiseError(new Exception("Internal Server Error"))

        override def getCustomer(customerId: String): IO[Option[RentCarModel.Customer]] = {
          if (customerId == "1") IO.pure(Some(customer))
          else if (customerId == "2") IO.pure(None)
          else IO.raiseError(new Exception("Internal Server Error"))
        }

        override def saveContract(contract: RentCarModel.Contract): IO[Unit] = IO.unit

        override def getContractByIdsAndEndDate(contract: Contract): IO[List[Contract]] = IO.pure(List.empty)

        override def getContractById(contractId: String): IO[Option[Contract]] = IO.pure(None)
      }
    }.route
  }
}
