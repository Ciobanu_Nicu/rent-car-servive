package rent.car

import akka.actor.ActorSystem
import spray.json.{JsValue, JsonReader, _}

import java.net.ServerSocket
import scala.io.Source

import scala.util.{Failure, Random, Success, Try}

object ItUtils {

  def getFreePort(): Int = {
    val serverSocket = new ServerSocket(0)
    val port         = serverSocket.getLocalPort
    serverSocket.close()
    port
  }

  def getRandomSchemaName(length: Int = 10): String =
    Random.alphanumeric.filter(_.isLetter).take(length).mkString

  def loadString(path: String)(implicit system: ActorSystem): String = {
    Try(Source.fromInputStream(getClass.getResourceAsStream(path), "utf-8").mkString) match {
      case Failure(ex: NullPointerException) =>
        system.log.error(s"Cannot find resource file from: $path")
        throw ex
      case Failure(ex) =>
        system.log.error(s"Failed to read file from $path. Message: ${ex.getMessage}")
        throw ex
      case Success(value) => value
    }
  }

  def loadJson(path: String)(implicit system: ActorSystem): JsValue = loadString(path).parseJson

  def loadEntity[T](path: String)(implicit reader: JsonReader[T], system: ActorSystem): T = loadJson(path).convertTo[T]
}
