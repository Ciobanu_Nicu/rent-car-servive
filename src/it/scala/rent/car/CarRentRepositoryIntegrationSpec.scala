package rent.car

import cats.effect.IO
import com.dimafeng.testcontainers.{Container, ForAllTestContainer}
import doobie.util.transactor.Transactor
import org.scalatest.GivenWhenThen
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import rent.car.database.CarRentRepository
import rent.car.model.RentCarModel.{BodyType, Car, CarBrand, Contract, Customer, FuelType}

import java.time.LocalDate
import java.time.format.DateTimeFormatter

class CarRentRepositoryIntegrationSpec extends AnyFlatSpec with GivenWhenThen with ForAllTestContainer with RentCarServiceProvider {

  override val container: Container = postgresContainer

  "CarRepository" should
    "store a car" in {
      withRentCarService(withPostgres = true) { implicit service =>
        implicit val transactor: Transactor[IO] = service.transactor

        val testSubject = CarRentRepository.instance(transactor)

        Given("a car")
        val car = Car(
          id = "1005",
          brand = CarBrand.Toyota,
          fuel = FuelType.Hybrid,
          body = BodyType.Sedans,
          year = LocalDate.now()
        )

        When("storing the car")
        val insertedCarResult = testSubject.saveCar(car).unsafeRunSync()

        And("the insertion result is successful")
        insertedCarResult shouldEqual ()

        Then("the car can be fetched from database")
        val storedCar = testSubject.getCar(car.id).unsafeRunSync()

        storedCar shouldBe Some(car)
      }
    }

  "CarRepository" should
    "store a customer" in {
    withRentCarService(withPostgres = true) { implicit service =>
      implicit val transactor: Transactor[IO] = service.transactor

      val testSubject = CarRentRepository.instance(transactor)

      Given("a customer")
      val customer = Customer(
        id = "99",
        firstName = "Dave",
        lastName = "Smith",
        birth = LocalDate.now()
      )

      When("storing the customer")
      val insertedCustomerResult = testSubject.saveCustomer(customer).unsafeRunSync()

      And("the insertion result is successful")
      insertedCustomerResult shouldEqual ()

      Then("the customer can be fetched from database")
      val storedCustomer = testSubject.getCustomer(customer.id).unsafeRunSync()

      storedCustomer shouldBe Some(customer)
    }
  }

  "CarRepository" should
    "return no car" in {
      withRentCarService(withPostgres = true) { implicit service =>
        implicit val transactor: Transactor[IO] = service.transactor

        val testSubject = CarRentRepository.instance(transactor)

        Given("a non existing car id")
        val nonExistingCarId = "9999"

        When("getting no existing car")
        val nonExistingCar = testSubject.getCar(nonExistingCarId).unsafeRunSync()

        Then("no values is returned")
        nonExistingCar shouldBe None
      }
    }

  "CarRepository" should
    "return no customer" in {
    withRentCarService(withPostgres = true) { implicit service =>
      implicit val transactor: Transactor[IO] = service.transactor

      val testSubject = CarRentRepository.instance(transactor)

      Given("a non existing customer id")
      val nonExistingCustomerId = "9999"

      When("getting non existing customer")
      val nonExistingCustomer = testSubject.getCustomer(nonExistingCustomerId).unsafeRunSync()

      Then("no values is returned")
      nonExistingCustomer shouldBe None
    }
  }

  "CarRepository" should
    "store a contract" in {
    withRentCarService(withPostgres = true) { implicit service =>
      implicit val transactor: Transactor[IO] = service.transactor

      val testSubject = CarRentRepository.instance(transactor)

      Given("a contract")
      val pattern = DateTimeFormatter.ofPattern("yyyy-MM-dd")

      val car = Car(
        id = "9999",
        brand = CarBrand.Toyota,
        fuel = FuelType.Hybrid,
        body = BodyType.Sedans,
        year = LocalDate.now()
      )

      val customer = Customer(
        id = "111",
        firstName = "Dave",
        lastName = "Smith",
        birth = LocalDate.now()
      )

      val contract = Contract(
        id = "2",
        customerId = customer.id,
        carId =car.id,
        startDate = LocalDate.parse("2025-01-01", pattern),
        endDate = LocalDate.parse("2025-01-07", pattern)
      )

      When("storing the contract")
      testSubject.saveCar(car).unsafeRunSync()
      testSubject.saveCustomer(customer).unsafeRunSync()

      val insertedContractResult = testSubject.saveContract(contract).unsafeRunSync()

      And("the insertion result is successful")
      insertedContractResult shouldEqual ()

      Then("the customer can be fetched from database")
      val storedContract = testSubject.getContractById(contract.id).unsafeRunSync()

      storedContract shouldBe Some(contract)
    }
  }

  "CarRepository" should
    "return no contract" in {
    withRentCarService(withPostgres = true) { implicit service =>
      implicit val transactor: Transactor[IO] = service.transactor

      val testSubject = CarRentRepository.instance(transactor)

      Given("a non existing customer and car id's")
      val pattern = DateTimeFormatter.ofPattern("yyyy-MM-dd")

      val contract = Contract(
        id = "111",
        customerId = "111",
        carId = "111",
        startDate = LocalDate.parse("2025-01-01", pattern),
        endDate = LocalDate.parse("2025-01-07", pattern)
      )

      When("getting an empty contract list")
      val emptyContractList = testSubject.getContractByIdsAndEndDate(contract).unsafeRunSync()

      Then("no values is returned")
      emptyContractList shouldBe List.empty
    }
  }
}
