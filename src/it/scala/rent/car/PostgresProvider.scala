package rent.car

import cats.effect.IO
import doobie.implicits._
import com.dimafeng.testcontainers.PostgreSQLContainer
import com.typesafe.config.ConfigFactory
import doobie.util.transactor.Transactor
import doobie.util.update.Update0

import scala.concurrent.ExecutionContext.global

trait PostgresProvider {

  val postgresContainer: PostgreSQLContainer = PostgreSQLContainer()
  implicit val contextShift                  = IO.contextShift(global)

  def createTransactor(schema: String): Transactor[IO] = Transactor.fromDriverManager[IO](
    driver = postgresContainer.driverClassName,
    url = s"${postgresContainer.jdbcUrl}&currentSchema=$schema&searchpath=$schema",
    user = postgresContainer.username,
    pass = postgresContainer.password
  )

  def initDatabase(schema: String, xa: Transactor[IO]): Unit = {
    (Update0(s"""CREATE SCHEMA $schema;""", None).run.transact(xa) *>
      rent.car.database.Migration.migrate(
        s"${postgresContainer.jdbcUrl}&currentSchema=$schema",
        postgresContainer.username,
        postgresContainer.password
      ))
      .unsafeRunSync()
  }

  def databaseConfig(schema: String) = {
    ConfigFactory
      .parseString(
        s"""
           | rent.car {
           |    database {
           |      url = "${postgresContainer.jdbcUrl}"
           |      user = "${postgresContainer.username}"
           |      password = "${postgresContainer.password}"
           |      max-pool-size = 10
           |      schema = "$schema"
           |    }
           |  }
           |""".stripMargin
      )
  }

  def generateSchema: String = ItUtils.getRandomSchemaName()
}
