package rent.car

import cats.effect.IO
import com.dimafeng.testcontainers.KafkaContainer
import com.typesafe.config.{Config, ConfigFactory}
import monix.execution.Ack.Continue
import monix.execution.Scheduler
import monix.kafka.config.AutoOffsetReset
import monix.kafka.{KafkaConsumerConfig, KafkaConsumerObservable, KafkaProducer, KafkaProducerConfig}
import org.apache.kafka.clients.consumer.ConsumerRecord

import scala.concurrent.{Await, Future, Promise}
import scala.util.Try
import scala.concurrent.duration.DurationInt

trait KafkaProvider {
  val kafkaContainer: KafkaContainer = KafkaContainer()

  def kafkaConfig(): Config =
    ConfigFactory
      .parseString(
      s"""
         |rent.car {
         |  kafka {
         |    bootstrap-servers = "${kafkaContainer.container.getBootstrapServers}"
         |  }
         |}
         """.stripMargin
      )

  def readFromKafka(topic: String): Try[String] = {
    val observable = createConsumerObservable(topic)

    val promise = Promise[String]()
    val obs = observable.subscribe { next =>
      promise.complete(Try(next.value()))
      Future.successful(Continue)
    }(monix.execution.Scheduler.global)

    Try(Await.result(promise.future, 10 seconds))
      .map { res =>
        obs.cancel()
        res
      }
      .recoverWith {
        case t: Throwable =>
          obs.cancel()
          throw t
      }
  }

  def sendMessagesToKafka(topic: String)(messages: String*)(implicit scheduler: Scheduler): Unit = {
    val producerConfig = KafkaProducerConfig
      .default
      .copy(bootstrapServers = List(kafkaContainer.container.getBootstrapServers))

    val producer = KafkaProducer[String, String](producerConfig, scheduler)

    messages.foreach { message =>
      producer
        .send(topic, "key", message)
        .to[IO]
        .unsafeRunSync()
    }

    producer.close().to[IO].unsafeRunSync()
  }

  def createConsumerObservable(topic: String): KafkaConsumerObservable[String, String, ConsumerRecord[String, String]] =
    KafkaConsumerObservable[String, String](
      KafkaConsumerConfig.default.copy(
        bootstrapServers = List(kafkaContainer.container.getBootstrapServers),
        groupId = "kafka-it-spec",
        autoOffsetReset = AutoOffsetReset.Earliest),
      List(topic))

}
