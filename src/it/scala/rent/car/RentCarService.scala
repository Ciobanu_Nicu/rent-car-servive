package rent.car

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.settings.ServerSettings
import akka.util.Timeout
import cats.effect.IO
import com.typesafe.config.{Config, ConfigFactory, ConfigResolveOptions}
import doobie.util.transactor.Transactor
import rent.car.configuration.{Settings, SettingsProvider}
import rent.car.database.{CarRentRepository, CarRepositoryProvider, DbTransactor}
import rent.car.routes.ApiRoutes
import cakemix.AskTimeoutProvider
import rent.car.configuration.ConfigModel.AppConfig
import rent.car.kafka.{RentCarRequestSubscriber, Retry}
import rent.car.service.RentRequestProcessorService

import scala.concurrent.Await

trait RentCarServiceProvider extends PostgresProvider with KafkaProvider {

  def withRentCarService(withPostgres: Boolean = false, withKafka: Boolean = false)(f: RentCarService => Unit): Unit = {
    val schema           = generateSchema
    val schemaTransactor = createTransactor(schema)

    def getConfig: Config = {
      val serviceConfig = ConfigFactory.parseString(
        s"""
           |rent.car.invoice {
           |  api.port = ${ItUtils.getFreePort()}
           |}
         """.stripMargin
      )

      ConfigFactory
        .empty()
        .withFallback(if (withPostgres) databaseConfig(schema) else ConfigFactory.empty())
        .withFallback(if (withKafka) kafkaConfig() else ConfigFactory.empty())
        .withFallback(serviceConfig)
        .withFallback(ConfigFactory.parseResources("application-it.conf"))
        .withFallback(ConfigFactory.parseResources("application.conf"))
        .resolve(ConfigResolveOptions.defaults.setAllowUnresolved(true))
    }

    val config = getConfig

    if (withPostgres) initDatabase(schema, schemaTransactor)

    val service = RentCarService(schemaTransactor, config)

    try f(service)
    finally service.shutdown()
  }
}

trait RentCarService extends ApiRoutes
  with SettingsProvider
  with DbTransactor[IO]
  with CarRepositoryProvider[IO]
  with RentCarRequestSubscriber {

  implicit def system: ActorSystem

  def askTimeout: Timeout
  def shutdown(): Unit
}

object RentCarService {
  def apply(dbTransactor: Transactor[IO], config: Config): RentCarService = {
    new RentCarService {
      override implicit lazy val system = ActorSystem("it-rent-car-service")
      implicit lazy val ec                  = system.dispatcher
      override implicit val settings   = Settings.instance
      implicit lazy val appConfig: AppConfig              = settings.load
      override implicit lazy val transactor               = dbTransactor

      val askTimeout: Timeout = AskTimeoutProvider.fromConfig(system.settings.config)

      override implicit lazy val carRentRepository = CarRentRepository.instance
      override implicit lazy val retry = Retry.instance
      override implicit lazy val rentRequestProcessorService = RentRequestProcessorService.instance

      val port           = settings.load.api.port
      val serverSettings = ServerSettings(system)

      val serverBinding = Http()
        .newServerAt(interface = "0.0.0.0", port)
        .withSettings(serverSettings)
        .bindFlow(route)

      def shutdown(): Unit = {
        Await.ready(serverBinding.flatMap(_.unbind()), askTimeout.duration)
        Await.ready(system.terminate(), askTimeout.duration)
      }
    }
  }
}
