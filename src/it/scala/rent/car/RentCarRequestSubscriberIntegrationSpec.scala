package rent.car

import com.dimafeng.testcontainers.{Container, ForAllTestContainer, MultipleContainers}

import monix.execution.Scheduler
import monix.kafka.config.AutoOffsetReset
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers.convertToAnyShouldWrapper
import org.scalatest.{GivenWhenThen, OptionValues}
import rent.car.model.RentCarJsonMarshalling.contractJsonFormat
import rent.car.model.RentCarModel.Contract
import spray.json._

class RentCarRequestSubscriberIntegrationSpec extends AnyFlatSpec
  with GivenWhenThen
  with ForAllTestContainer
  with KafkaProvider
  with PostgresProvider
  with OptionValues
  with RentCarServiceProvider {

  override val container: Container = MultipleContainers(postgresContainer, kafkaContainer)

  "Rent car service subscriber" should
    "consume rent car contract request message" in {
      withRentCarService(withPostgres = true, withKafka = true) { implicit service =>

        implicit val settings = service.settings
        implicit val system = service.system
        implicit val scheduler: Scheduler = Scheduler.io("kafka")

        val rentCarRequestTopic = settings.load.kafka.rentCarContractRequest.topic

        Given("a valid contract request message")
        val contractStrMessage = ItUtils.loadString("/kafka/contract-request.json")
        val contract = ItUtils.loadEntity[Contract]("/kafka/contract-request.json")

        When("the rent car service subscribe to topic and consume messages")
        val subscription = service.subscribeToRentRequestTopic(AutoOffsetReset.Earliest)

        sendMessagesToKafka(rentCarRequestTopic)(contractStrMessage)

        Then("the contract should be read by kafka")
        val kafkaContract = readFromKafka(rentCarRequestTopic).get.parseJson.convertTo[Contract]

        kafkaContract shouldBe contract

        subscription.cancel()
      }
  }
}
