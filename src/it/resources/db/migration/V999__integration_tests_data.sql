INSERT INTO car(id, brand, fuel, body, year)
VALUES ('1001', 'Mercedes', 'Hybrid', 'OffRoad', '01-01-2020');

INSERT INTO car(id, brand, fuel, body, year)
VALUES ('1002', 'Toyota', 'Diesel', 'Sedans', '01-01-2020');

INSERT INTO car(id, brand, fuel, body, year)
VALUES ('1003', 'Opel', 'Gasoline', 'Coupe', '01-01-2021');

INSERT INTO customer(id, first_name, last_name, birth)
VALUES ('1', 'David', 'Smith', '01-01-1988');

INSERT INTO customer(id, first_name, last_name, birth)
VALUES ('3', 'John', 'Thomson', '01-01-1988');

INSERT INTO contract(id, customer_id, car_id, start_date, end_date)
VALUES ('1', '1', '1001', '01-01-2025', '07-01-2025');