package rent.car.model

import rent.car.util._

import java.time.LocalDate

object RentCarModel {

  case class Car(id: String, brand: CarBrand, fuel: FuelType, body: BodyType, year: LocalDate)

  case class Customer(id: String, firstName: String, lastName: String, birth: LocalDate)

  case class Contract(id: String, customerId: String, carId: String, startDate: LocalDate, endDate: LocalDate)

  sealed trait CarBrand extends EnumValue

  object CarBrand extends Enum[CarBrand] {
    case object Opel     extends CarBrand
    case object Toyota   extends CarBrand
    case object Mercedes extends CarBrand

    override val values: Set[CarBrand] = Set(Opel, Toyota, Mercedes)
  }

  sealed trait FuelType extends EnumValue

  object FuelType extends Enum[FuelType] {
    case object Gasoline extends FuelType
    case object Diesel   extends FuelType
    case object Hybrid   extends FuelType

    override val values: Set[FuelType] = Set(Gasoline, Diesel, Hybrid)
  }

  sealed trait BodyType extends EnumValue

  object BodyType extends Enum[BodyType] {
    case object Coupe   extends BodyType
    case object Sedans  extends BodyType
    case object OffRoad extends BodyType

    override val values: Set[BodyType] = Set(Coupe, Sedans, OffRoad)
  }
}
