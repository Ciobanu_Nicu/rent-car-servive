package rent.car.model

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import rent.car.model.RentCarModel._
import rent.car.util.EnumJsonMarshalling

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import spray.json._

object RentCarJsonMarshalling extends SprayJsonSupport with DefaultJsonProtocol with EnumJsonMarshalling {

  implicit def localDateJsonFormat: JsonFormat[LocalDate] =
    new JsonFormat[LocalDate] {
      private val formatter                             = DateTimeFormatter.ISO_DATE
      override def write(localDate: LocalDate): JsValue = JsString(localDate.format(formatter))

      override def read(value: JsValue): LocalDate = value match {
        case JsString(x) => LocalDate.parse(x)
        case x           => serializationError(s"LocalDate deserialization error: $x")
      }
    }

  implicit def carBrandJsonFormat: JsonFormat[CarBrand] = enumJsonFormat(CarBrand)
  implicit def fuelTypeJsonFormat: JsonFormat[FuelType] = enumJsonFormat(FuelType)
  implicit def bodyTypeJsonFormat: JsonFormat[BodyType] = enumJsonFormat(BodyType)

  implicit val carJsonFormat      = jsonFormat5(Car)
  implicit val customerJsonFormat = jsonFormat4(Customer)
  implicit val contractJsonFormat = jsonFormat5(Contract)

}
