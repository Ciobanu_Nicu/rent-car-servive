package rent.car.model

import rent.car.model.RentCarModel.Contract

object RentCarProcessorErrorModel {

  trait RentCarServiceError extends Throwable

  case class ContractorNotFoundError(contractorId: String) extends RentCarServiceError {
    override def getMessage: String = s"Contractor not found error: $contractorId"
  }

  case class CarNotFoundError(carId: String) extends RentCarServiceError {
    override def getMessage: String = s"Car not found error: $carId"
  }

  case class ParseContractStrError(contractStr: String) extends RentCarServiceError {
    override def getMessage: String = s"Parse contract string error: $contractStr"
  }

  case class SaveContractError(contract: Contract) extends RentCarServiceError {
    override def getMessage: String = s"Save contract error: $contract"
  }

  case class ContractNotValidError(carId: String) extends RentCarServiceError {
    override def getMessage: String = s"Time intervals overlaps for car: $carId"
  }
}
