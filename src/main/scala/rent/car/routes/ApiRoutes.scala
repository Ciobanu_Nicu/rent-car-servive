package rent.car.routes

import akka.actor.ActorSystem
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import akka.http.scaladsl.server.{Directives, Route}
import akka.http.scaladsl.model.StatusCodes.{InternalServerError, NotFound, OK}
import cats.effect.IO
import rent.car.database.CarRepositoryProvider
import rent.car.model.RentCarModel.{Car, Customer}
import rent.car.model.RentCarJsonMarshalling._
import spray.json._

trait ApiRoutes extends Directives with SprayJsonSupport with CarRepositoryProvider[IO] {

  implicit lazy val system: ActorSystem = ActorSystem("car-service")

  val route: Route =
    pathPrefix("api") {
      addNewCar ~ addNewCustomer ~ searchForCar ~ searchForCustomer
    }

  def addNewCar: Route =
    (path("add" / "car") & post) {
      entity(as[Car]) { car =>
        carRentRepository.saveCar(car).attempt.unsafeRunSync() match {
          case Left(exception) =>
            system.log.error(s"Exception while adding car ${car.id}", exception)
            complete(InternalServerError)
          case Right(_) =>
            system.log.info(s"Car ${car.id} added with success")
            complete(OK)
        }
      }
    }

  def addNewCustomer: Route =
    (path("add" / "customer") & post) {
      entity(as[Customer]) { customer =>
        carRentRepository.saveCustomer(customer).attempt.unsafeRunSync() match {
          case Left(exception) =>
            system.log.error(s"Exception while adding customer ${customer.id}", exception)
            complete(InternalServerError)
          case Right(_) =>
            system.log.info(s"Customer ${customer.id} added with success")
            complete(OK)
        }
      }
    }

  def searchForCar: Route =
    (path("get" / Segment / "car") & get) { carId: String =>
      carRentRepository.getCar(carId).attempt.unsafeRunSync() match {
        case Left(exception: Throwable) =>
          system.log.error(s"Exception while getting car $carId", exception)
          complete(InternalServerError)
        case Right(None) =>
          system.log.info(s"Car $carId does not exist")
          complete(NotFound)
        case Right(Some(car)) =>
          val response = car.toJson.compactPrint
          system.log.info(s"Car $carId retrieved with success")
          complete(OK, response)
      }
    }

  def searchForCustomer: Route =
    (path("get" / Segment / "customer") & get) { customerId: String =>
      carRentRepository.getCustomer(customerId).attempt.unsafeRunSync() match {
        case Left(exception: Throwable) =>
          system.log.error(s"Exception while getting customer $customerId", exception)
          complete(InternalServerError)
        case Right(None) =>
          system.log.info(s"Customer $customerId does not exist")
          complete(NotFound)
        case Right(Some(car)) =>
          val response = car.toJson.compactPrint
          system.log.info(s"Customer $customerId retrieved with success")
          complete(OK, response)
      }
    }
}
