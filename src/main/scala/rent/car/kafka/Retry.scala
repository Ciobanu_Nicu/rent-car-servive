package rent.car.kafka

import akka.actor.ActorSystem
import cats.effect.{IO, Timer}

import retry.RetryDetails.WillDelayAndRetry
import retry.RetryPolicies._
import retry._
import simulacrum.typeclass

import scala.concurrent.ExecutionContext.global
import scala.concurrent.duration._

@typeclass
trait Retry[F[_]] {
  def callWithRetries[R](maxRetries: Int, backoff: FiniteDuration, isExceptionWorthRetrying: Throwable => Boolean = _ => true)(
      errorMessage: String
  )(function: => F[R]): F[R]
}

object Retry {
  implicit def instance(implicit system: ActorSystem): Retry[IO] = new Retry[IO] {
    override def callWithRetries[R](maxRetries: Int, backoff: FiniteDuration, isExceptionWorthRetrying: Throwable => Boolean)(
        errorMessage: String
    )(function: => IO[R]): IO[R] = {
      implicit val timer: Timer[IO] = IO.timer(global)

      retryingOnSomeErrors[R].apply[IO, Throwable](
        policy = exponentialBackoff[IO](backoff) join limitRetries(maxRetries),
        isWorthRetrying = isExceptionWorthRetrying,
        onError = (t: Throwable, r: RetryDetails) =>
          r match {
            case WillDelayAndRetry(nextDelay, retriesSoFar, _) =>
              IO(
                system.log.info(
                  s"$errorMessage: ${t.getMessage}. " + s"Will retry ${maxRetries - retriesSoFar} more times, after $nextDelay"
                )
              )
            case RetryDetails.GivingUp(_, _) =>
              IO(system.log.error(s"$errorMessage: ${t.getMessage}. " + s"Giving up after $maxRetries retries"))
          }
      )(function)
    }
  }
}

trait RetryProvider[F[_]] {
  def retry: Retry[F]
}
