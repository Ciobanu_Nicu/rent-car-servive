package rent.car.kafka

import akka.actor.ActorSystem
import cats.effect.IO
import monix.eval.Task
import rent.car.configuration.SettingsProvider
import monix.execution.{Cancelable, Scheduler}
import monix.kafka.{KafkaConsumerConfig, KafkaConsumerObservable}
import monix.kafka.config.AutoOffsetReset
import rent.car.configuration.ConfigModel.{KafkaConsumerConfig => KafkaSettings}

trait KafkaClient extends SettingsProvider {

  protected def subscribe(
      consumer: String => IO[Unit],
      consumerSettings: KafkaSettings,
      autoOffsetReset: AutoOffsetReset = AutoOffsetReset.Latest
  )(implicit
      system: ActorSystem
  ): Cancelable = {
    val consumerConfig: KafkaConsumerConfig = createKafkaConsumerConfig(consumerSettings.groupId, autoOffsetReset)
    implicit val scheduler: Scheduler       = monix.execution.Scheduler.io()

    KafkaConsumerObservable[String, String](consumerConfig, List(consumerSettings.topic))
      .mapEval { record =>
        consumer(record.value())
          .handleErrorWith(throwable =>
            IO(
              system.log
                .error(
                  s"Unexpected error while consuming kafka topic ${consumerSettings.topic} with message ${record.value()}",
                  throwable.getMessage
                )
            )
          )
          .to[Task]
      }
      .subscribe()
  }

  def createKafkaConsumerConfig(groupId: String, autoOffsetReset: AutoOffsetReset): KafkaConsumerConfig =
    KafkaConsumerConfig.default.copy(
      bootstrapServers = List(settings.load.kafka.bootstrapServers),
      groupId = groupId,
      autoOffsetReset = autoOffsetReset
    )
}
