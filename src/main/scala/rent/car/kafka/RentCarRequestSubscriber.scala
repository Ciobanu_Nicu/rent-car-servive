package rent.car.kafka

import akka.actor.ActorSystem
import cats.effect.IO
import monix.execution.Cancelable
import monix.kafka.config.AutoOffsetReset
import rent.car.configuration.SettingsProvider
import rent.car.model.RentCarProcessorErrorModel.RentCarServiceError
import rent.car.service.RentRequestProcessorServiceProvider

import scala.concurrent.duration.FiniteDuration

trait RentCarRequestSubscriber
    extends KafkaClient
    with SettingsProvider
    with RentRequestProcessorServiceProvider[IO]
    with RetryProvider[IO] {
  def subscribeToRentRequestTopic(autoOffsetReset: AutoOffsetReset = AutoOffsetReset.Latest)(implicit system: ActorSystem): Cancelable = {
    val kafkaConfig = settings.load.kafka.rentCarContractRequest
    subscribe(
      processRentRequestWithRetries(maxRetries = kafkaConfig.maxRetries, backoff = kafkaConfig.backoff, kafkaConfig.topic),
      kafkaConfig,
      autoOffsetReset
    )
  }

  private def processRentRequestWithRetries(maxRetries: Int, backoff: FiniteDuration, topic: String)(message: String)(implicit
      system: ActorSystem
  ): IO[Unit] = {
    retry.callWithRetries(maxRetries, backoff, isExceptionWorthRetrying)(s"Failed to process message $message from topic $topic")(
      rentRequestProcessorService.processRentRequest(message)
    )
  }

  private def isExceptionWorthRetrying(e: Throwable): Boolean = e match {
    case _: RentCarServiceError => false
    case _                      => true
  }
}
