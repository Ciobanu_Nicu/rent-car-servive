package rent.car.database

import cats.effect.{Blocker, IO}
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import doobie.hikari.HikariTransactor
import doobie.util.transactor.Transactor
import rent.car.configuration.ConfigModel.AppConfig

import scala.concurrent.ExecutionContext

trait DbTransactor[F[_]] {
  def transactor: Transactor[F]
}

object DbTransactor {

  implicit def instance(implicit appConfig: AppConfig, ec: ExecutionContext): DbTransactor[IO] = new DbTransactor[IO] {
    override def transactor: Transactor[IO] = {
      implicit val contextShift = IO.contextShift(ec)
      HikariTransactor(new HikariDataSource(getDatabaseConfig(appConfig)), ec, Blocker.liftExecutionContext(ec))
    }
  }

  def getDatabaseConfig(appConfig: AppConfig): HikariConfig = {
    val dbConfig = appConfig.database
    val config   = new HikariConfig()
    config.setDriverClassName("org.postgresql.Driver")
    config.setJdbcUrl(dbConfig.url)
    config.setUsername(dbConfig.user)
    config.setPassword(dbConfig.password)
    config.setMaximumPoolSize(dbConfig.maxPoolSize)
    config
  }
}

trait DbTransactorProvider[F[_]] {
  val dbTransactor: DbTransactor[F]
}
