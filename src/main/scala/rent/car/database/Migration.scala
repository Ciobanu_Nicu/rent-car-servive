package rent.car.database

import cats.effect.IO
import org.flywaydb.core.Flyway

object Migration {
  def migrate(jdbcUrl: String, user: String, password: String): IO[Unit] = {
    IO {
      Flyway
        .configure()
        .dataSource(jdbcUrl, user, password)
        .load()
        .migrate()
    }
  }
}
