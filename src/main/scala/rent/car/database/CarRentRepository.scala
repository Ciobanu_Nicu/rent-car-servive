package rent.car.database

import cats.effect.IO
import doobie.Transactor
import doobie.implicits._
import doobie.postgres.implicits._
import rent.car.model.RentCarModel.{Car, Contract, Customer}
import MetaMappings._
import simulacrum.typeclass

@typeclass
trait CarRentRepository[F[_]] {
  def saveCar(car: Car): F[Unit]
  def getCar(carId: String): F[Option[Car]]
  def saveCustomer(customer: Customer): F[Unit]
  def getCustomer(customerId: String): F[Option[Customer]]
  def saveContract(contract: Contract): F[Unit]
  def getContractByIdsAndEndDate(contract: Contract): F[List[Contract]]
  def getContractById(contractId: String): IO[Option[Contract]]
}

object CarRentRepository {
  implicit def instance(implicit xa: Transactor[IO]): CarRentRepository[IO] = new CarRentRepository[IO] {
    override def saveCar(car: Car): IO[Unit] = {
      val query =
        sql"""
            INSERT INTO car (
              id,
              brand,
              fuel,
              body,
              year
            ) VALUES (
              ${car.id},
              ${car.brand},
              ${car.fuel},
              ${car.body},
              ${car.year}
            )
          """.stripMargin.update.run

      query.transact(xa).map(_ => ())
    }

    override def getCar(carId: String): IO[Option[Car]] = {
      val query =
        sql"""SELECT
             id,
             brand,
             fuel,
             body,
             year
             FROM car
             WHERE id = $carId
             """.stripMargin
          .query[Car]
          .option

      query.transact(xa)
    }

    override def saveCustomer(customer: Customer): IO[Unit] = {
      val query =
        sql"""
            INSERT INTO customer (
              id,
              first_name,
              last_name,
              birth
            ) VALUES (
              ${customer.id},
              ${customer.firstName},
              ${customer.lastName},
              ${customer.birth}
            )
          """.stripMargin.update.run

      query.transact(xa).map(_ => ())
    }

    override def getCustomer(customerId: String): IO[Option[Customer]] = {
      val query =
        sql"""SELECT
             id,
             first_name,
             last_name,
             birth
             FROM customer
             WHERE id = $customerId
             """.stripMargin
          .query[Customer]
          .option

      query.transact(xa)
    }

    override def saveContract(contract: Contract): IO[Unit] = {
      val query =
        sql"""
            INSERT INTO contract (
              id,
              customer_id,
              car_id,
              start_date,
              end_date
            ) VALUES (
              ${contract.id},
              ${contract.customerId},
              ${contract.carId},
              ${contract.startDate},
              ${contract.endDate}
            )
          """.stripMargin.update.run

      query.transact(xa).map(_ => ())
    }

    override def getContractByIdsAndEndDate(contract: Contract): IO[List[Contract]] = {
      val query =
        sql"""SELECT
             id,
             customer_id,
             car_id,
             start_date,
             end_date
             FROM contract
             WHERE
             customer_id = ${contract.customerId}
             AND car_id = ${contract.carId}
             AND start_date > end_date
             ORDER BY end_date DESC
             """.stripMargin
          .query[Contract]
          .to[List]

      query.transact(xa)
    }

    override def getContractById(contractId: String): IO[Option[Contract]] = {
      val query =
        sql"""SELECT
             id,
             customer_id,
             car_id,
             start_date,
             end_date
             FROM contract
             WHERE
             id = $contractId
             """.stripMargin
          .query[Contract]
          .option

      query.transact(xa)
    }
  }
}

trait CarRepositoryProvider[F[_]] {
  val carRentRepository: CarRentRepository[F]
}
