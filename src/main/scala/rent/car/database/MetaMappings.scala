package rent.car.database

import doobie.Meta
import rent.car.model.RentCarModel._

object MetaMappings {

  implicit val carBrandMeta: Meta[CarBrand] =
    Meta[String].timap(name => CarBrand.valueOf(name).get)(carBrand => carBrand.name)

  implicit val fuelTypeMeta: Meta[FuelType] =
    Meta[String].timap(name => FuelType.valueOf(name).get)(fuelType => fuelType.name)

  implicit val bodyTypeMeta: Meta[BodyType] =
    Meta[String].timap(name => BodyType.valueOf(name).get)(bodyType => bodyType.name)

}
