package rent.car

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.settings.ServerSettings
import cats.effect.IO
import rent.car.configuration.ConfigModel.AppConfig
import rent.car.configuration.{Settings, SettingsProvider}
import rent.car.database._
import rent.car.kafka.{RentCarRequestSubscriber, Retry, RetryProvider}
import rent.car.routes.ApiRoutes
import rent.car.service.RentRequestProcessorService

import scala.concurrent.ExecutionContextExecutor
import scala.concurrent.duration.DurationInt

object Main
    extends App
    with SettingsProvider
    with DbTransactorProvider[IO]
    with CarRepositoryProvider[IO]
    with RetryProvider[IO]
    with RentCarRequestSubscriber
    with ApiRoutes {

  implicit override lazy val system: ActorSystem = ActorSystem("rent-car-service")
  implicit val ec: ExecutionContextExecutor      = system.dispatcher

  implicit override val settings: Settings = Settings.instance
  implicit val appConfig: AppConfig        = settings.load
  implicit override val dbTransactor       = DbTransactor.instance(appConfig, ec)

  implicit override val carRentRepository  = CarRentRepository.instance(dbTransactor.transactor)
  override val retry                       = Retry.instance
  override val rentRequestProcessorService = RentRequestProcessorService.instance

  val db              = appConfig.database
  val migrationResult = database.Migration.migrate(db.url, db.user, db.password).attempt.unsafeRunSync()

  migrationResult match {
    case Left(exception) => system.log.error("Exception while migrating schema", exception)
    case Right(result) =>
      system.log.info("Schema migrated with success", result)

      val serverSettings = ServerSettings(system)

      system.scheduler.scheduleOnce(10 milliseconds) {
        subscribeToRentRequestTopic()
      }

      Http()
        .newServerAt(interface = "0.0.0.0", port = appConfig.api.port)
        .withSettings(serverSettings)
        .bindFlow(route)
  }
}
