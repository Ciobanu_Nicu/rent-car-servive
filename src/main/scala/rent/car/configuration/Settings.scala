package rent.car.configuration

import com.typesafe.config.ConfigFactory
import pureconfig.ConfigSource
import pureconfig.error.ConfigReaderFailures
import pureconfig.generic.auto._
import rent.car.configuration.ConfigModel._

trait Settings {
  def load: AppConfig
}

object Settings {
  val instance = new Settings {
    override def load: AppConfig =
      config.fold(err => throw ConfigError(err), conf => conf)
  }

  private def config: Either[ConfigReaderFailures, AppConfig] =
    ConfigSource
      .fromConfig(
        ConfigFactory
          .load("application.conf")
          .getConfig("rent.car")
      )
      .load[AppConfig]
}

trait SettingsProvider {
  val settings: Settings
}
