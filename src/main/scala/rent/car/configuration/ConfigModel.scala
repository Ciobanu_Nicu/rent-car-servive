package rent.car.configuration

import pureconfig.error._

import scala.concurrent.duration.FiniteDuration

object ConfigModel {

  case class AppConfig(api: Api, database: Database, kafka: Kafka)

  case class Api(port: Int)

  case class Database(url: String, user: String, password: String, maxPoolSize: Int, schema: String)

  case class Kafka(
      bootstrapServers: String,
      rentCarContractRequest: KafkaConsumerConfig
  )

  case class KafkaConsumerConfig(topic: String, groupId: String, maxRetries: Int, backoff: FiniteDuration)

  case class ConfigError(failure: ConfigReaderFailures) extends Exception {

    private def failureToString(failure: ConfigReaderFailure) = failure match {
      case _: CannotParse | _: CannotReadFile => failure.description
      case f: ConvertFailure                  => s"${f.description} in ${f.path}"
      case f: ThrowableFailure                => s"${f.description} \n ${f.throwable.getMessage}"
    }

    override def getMessage: String = failure.toList.map(failureToString).mkString("\n")
  }

}
