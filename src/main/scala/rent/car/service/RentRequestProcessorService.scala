package rent.car.service

import akka.actor.ActorSystem
import cats.effect.IO
import rent.car.database.CarRentRepository
import rent.car.model.RentCarModel.Contract
import simulacrum.typeclass
import spray.json._

@typeclass
trait RentRequestProcessorService[F[_]] {
  def processRentRequest(rentRequestMessage: String): F[Unit]
}

object RentRequestProcessorService {
  import rent.car.model.RentCarJsonMarshalling._
  import rent.car.model.RentCarProcessorErrorModel._

  implicit def instance(implicit system: ActorSystem, carRentRepository: CarRentRepository[IO]): RentRequestProcessorService[IO] =
    new RentRequestProcessorService[IO] {
      override def processRentRequest(rentRequestMessage: String): IO[Unit] =
        for {
          contract <- parseContractStr(rentRequestMessage)
          _        <- checkNewRentCarKafkaContractRequest(contract)
          _        <- IO(system.log.info(s"Saving contract: $contract"))
          _        <- saveContract(contract)
        } yield ()

      def checkNewRentCarKafkaContractRequest(
          contract: Contract
      )(implicit system: ActorSystem, carRentRepository: CarRentRepository[IO]): IO[Unit] =
        for {
          _ <- checkContractor(contract.customerId)
          _ <- checkCar(contract.carId)
          _ <- checkContract(contract)
        } yield ()

      def checkContractor(customerId: String)(implicit system: ActorSystem, carRentRepository: CarRentRepository[IO]): IO[Unit] =
        for {
          _        <- IO(system.log.info(s"Searching for customer: $customerId"))
          customer <- carRentRepository.getCustomer(customerId)
          _        <- IO.raiseWhen(customer.isEmpty)(ContractorNotFoundError(customerId))
        } yield ()

      def checkCar(carId: String)(implicit system: ActorSystem, carRentRepository: CarRentRepository[IO]): IO[Unit] =
        for {
          _   <- IO(system.log.info(s"Searching for car: $carId"))
          car <- carRentRepository.getCar(carId)
          _   <- IO.raiseWhen(car.isEmpty)(CarNotFoundError(carId))
        } yield ()

      def parseContractStr(contractStr: String)(implicit system: ActorSystem, carRentRepository: CarRentRepository[IO]): IO[Contract] =
        for {
          _ <- IO(system.log.info(s"Parsing contract: $contractStr"))
          contract <- IO(contractStr.parseJson.convertTo[Contract])
            .handleErrorWith(_ => IO.raiseError(ParseContractStrError(contractStr)))
        } yield contract

      def checkContract(contract: Contract)(implicit system: ActorSystem, carRentRepository: CarRentRepository[IO]): IO[Unit] =
        for {
          _         <- IO(system.log.info(s"Searching for contract: $contract"))
          contracts <- carRentRepository.getContractByIdsAndEndDate(contract)
          _         <- IO.raiseWhen(contracts.nonEmpty)(ContractNotValidError(contract.carId))
        } yield ()

      def saveContract(contract: Contract)(implicit system: ActorSystem, carRentRepository: CarRentRepository[IO]): IO[Unit] =
        for {
          _ <- IO(system.log.info(s"Saving contract: $contract"))
          _ <- carRentRepository
            .saveContract(contract)
            .handleErrorWith(_ => IO.raiseError(SaveContractError(contract)))
        } yield ()
    }
}

trait RentRequestProcessorServiceProvider[F[_]] {
  val rentRequestProcessorService: RentRequestProcessorService[F]
}
