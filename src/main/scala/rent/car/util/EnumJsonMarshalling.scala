package rent.car.util

import spray.json._

trait EnumJsonMarshalling {

  implicit def enumJsonFormat[A <: EnumValue](enum: Enum[A]): JsonFormat[A] = new JsonFormat[A] {
    override def write(obj: A): JsValue = JsString(obj.name)

    override def read(json: JsValue): A = json match {
      case JsString(name) =>
        `enum`
          .valueOf(name)
          .getOrElse(deserializationError(s"Expected ${`enum`.values.mkString(", ")}, but got $name"))

      case other =>
        deserializationError(s"Expected ${ClassNameExtractor(`enum`.getClass)} as JsString, but got $other")
    }
  }

}
