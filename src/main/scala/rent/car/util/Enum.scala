package rent.car.util

trait Enum[A <: EnumValue] {

  val values: Set[A]

  def valueOf(name: String): Option[A] = enumValueByName.get(name.toLowerCase)

  private lazy val enumValueByName: Map[String, A] = values.map(item => item.name.toLowerCase -> item).toMap

}

trait EnumValue {
  def name: String = ClassNameExtractor(getClass)
}

object ClassNameExtractor {
  def getClassName(input: String): String = input.split("\\.").last.split("\\$").last
  def apply[A](input: Class[A]): String   = getClassName(input.getName)
}
