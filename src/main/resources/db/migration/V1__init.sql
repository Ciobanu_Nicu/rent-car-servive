CREATE TABLE car
(
    id VARCHAR(50) PRIMARY KEY,
    brand VARCHAR(50) NOT NULL,
    fuel VARCHAR(50) NOT NULL,
    body VARCHAR(50) NOT NULL,
    year Date NOT NULL
);

CREATE TABLE customer
(
    id VARCHAR(50) PRIMARY KEY,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    birth Date NOT NULL
);

CREATE TABLE contract
(
    id VARCHAR(50) PRIMARY KEY,
    customer_id VARCHAR(50) NOT NULL,
    car_id VARCHAR(50) NOT NULL,
    start_date Date NOT NULL,
    end_date Date NOT NULL
);