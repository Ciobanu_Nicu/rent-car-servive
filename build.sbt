lazy val root = Project("rent-car-service", file("."))
  .settings(
    scalacOptions ++= Seq(
      "-encoding",
      "utf8",
      "-Ymacro-annotations",
      "-deprecation",
      "-unchecked",
      "-feature",
      "-language:implicitConversions",
      "-language:higherKinds",
      "-language:existentials",
      "-language:postfixOps"
    )
  )
  .configs(IntegrationTest)
  .settings(Defaults.itSettings)
  .settings(
    scalaVersion := "2.13.6",
    mainClass.withRank(KeyRanks.Invisible) := Some("rent.car.Main"),
    libraryDependencies ++=
      Dependencies.compile ++
        Dependencies.runtime ++
        Dependencies.test ++
        Dependencies.it
  )
  .settings(
    IntegrationTest / fork := true
  )
