import sbt._

object Dependencies {

  val doobieVersion         = "0.13.4"
  val akkaVersion           = "2.6.14"
  val akkaHttpVersion       = "10.2.4"
  val testcontainersVersion = "0.39.3"
  val kamonVersion          = "2.1.17"

  val compile = Seq(
    "org.typelevel"         %% "cats-core"            % "2.6.0",
    "org.typelevel"         %% "cats-effect"          % "2.4.1",
    "com.olegpy"            %% "meow-mtl"             % "0.3.0-M1",
    "org.flywaydb"           % "flyway-core"          % "6.5.7",
    "org.tpolecat"          %% "doobie-core"          % doobieVersion,
    "org.tpolecat"          %% "doobie-postgres"      % doobieVersion,
    "org.tpolecat"          %% "doobie-hikari"        % doobieVersion,
    "com.typesafe.akka"     %% "akka-actor"           % akkaVersion,
    "com.typesafe.akka"     %% "akka-stream"          % akkaVersion,
    "com.typesafe.akka"     %% "akka-http"            % akkaHttpVersion,
    "com.typesafe.akka"     %% "akka-http-spray-json" % akkaHttpVersion,
    "com.github.pureconfig" %% "pureconfig"           % "0.15.0",
    "nl.wehkamp.cakemix"    %% "cakemix"              % "1.3.0",
    "io.kamon"              %% "kamon-logback"        % kamonVersion,
    "io.monix"              %% "monix-kafka-1x"       % "1.0.0-RC7",
    "com.github.cb372"      %% "cats-retry"           % "2.1.1"
  )

  val runtime = Seq(
    "ch.qos.logback"       % "logback-classic"          % "1.2.3",
    "net.logstash.logback" % "logstash-logback-encoder" % "6.6",
    "org.codehaus.janino"  % "janino"                   % "3.1.3"
  )

  val test = Seq(
    "com.typesafe.akka" %% "akka-testkit"        % akkaVersion,
    "com.typesafe.akka" %% "akka-http-testkit"   % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-stream-testkit" % akkaVersion,
    "org.scalatest"     %% "scalatest"           % "3.2.8" % "it,test",
    "org.scalatestplus" %% "scalacheck-1-14"     % "3.2.2.0",
    "org.scalacheck"    %% "scalacheck"          % "1.15.4",
    "org.scalamock"     %% "scalamock"           % "5.1.0"
  )

  val it = Seq(
    "com.dimafeng" %% "testcontainers-scala-scalatest"  % testcontainersVersion,
    "com.dimafeng" %% "testcontainers-scala-postgresql" % testcontainersVersion,
    "com.dimafeng" %% "testcontainers-scala-kafka"      % testcontainersVersion
  )
}
